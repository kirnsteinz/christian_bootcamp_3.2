import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {
  private roomList : Object[] = [
    {"Id":"1","NoKamar":"101","Harga":"100000","Status": 1},
    {"Id":"2","NoKamar":"201","Harga":"200000","Status": 1},
    {"Id":"3","NoKamar":"301","Harga":"300000","Status": 1},
    {"Id":"4","NoKamar":"401","Harga":"400000","Status": 1},
    {"Id":"5","NoKamar":"501","Harga":"500000","Status": 1},
    {"Id":"6","NoKamar":"601","Harga":"600000","Status": 1},
    {"Id":"7","NoKamar":"701","Harga":"700000","Status": 1}
  ]
  constructor() { }

  ngOnInit() {
  }

  book(index){
    this.roomList.splice(index, 1);
  }
}
