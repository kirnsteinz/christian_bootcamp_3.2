import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomListComponent } from './room-list/room-list.component';
import { BookedListComponent } from './booked-list/booked-list.component';
import { EditPriceComponent } from './edit-price/edit-price.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { StayingCustomerComponent } from './staying-customer/staying-customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { NewOrderComponent } from './new-order/new-order.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomListComponent,
    BookedListComponent,
    EditPriceComponent,
    CustomerListComponent,
    StayingCustomerComponent,
    AddCustomerComponent,
    NewOrderComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
