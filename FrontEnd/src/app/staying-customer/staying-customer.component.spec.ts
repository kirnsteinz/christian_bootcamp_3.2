import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StayingCustomerComponent } from './staying-customer.component';

describe('StayingCustomerComponent', () => {
  let component: StayingCustomerComponent;
  let fixture: ComponentFixture<StayingCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StayingCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StayingCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
