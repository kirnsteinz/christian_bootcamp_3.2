import { NewprojectPage } from './app.po';

describe('newproject App', () => {
  let page: NewprojectPage;

  beforeEach(() => {
    page = new NewprojectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
