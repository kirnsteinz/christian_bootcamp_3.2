<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kamar;
use App\Order;
use Illuminate\Support\Facades\DB;

class KamarController extends Controller
{
    function book(Request $req){
        DB::beginTransaction();
        try{
            $order = new Order;
            $order->CustomerID = $req->input('CustomerID');
            $order->KamarID = $req->input('KamarID');
            // $order->CheckIn = $req->input('CheckIn');
            // $order->CheckOut = $req->input('CheckOut');
            $order->save();
            $kamar = Kamar::get()->where('Id',$req->input('KamarID'))->first();
            $kamar->status = 0;
            $kamar->save();
            DB::commit();
            return response()->json(["message"=>"success"], 200);            
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=>$e->getMessage()], 500);
        } 
    }
}
