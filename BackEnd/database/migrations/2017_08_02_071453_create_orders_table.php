<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->date('CheckIn');
            $table->date('CheckOut');
            $table->integer('CustomerID')->unsigned();
            $table->foreign('CustomerID')->references('Id')->on('Customers');
            $table->integer('KamarID')->unsigned();
            $table->foreign('KamarID')->references('Id')->on('Kamars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
